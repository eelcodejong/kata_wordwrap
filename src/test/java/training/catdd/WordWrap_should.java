package training.catdd;

/**
 * Created by jongd on 10-May-17.
 */

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * TODO: Return single word | ("de", 2) >> "de"
 * TODO: Return split word | ("de", 1) >> "d\ne"
 * TODO: Replace space with newline | ("de wielen", 2) >> "de\nwielen"
 * TODO: Split on space | ("de wielen", 5) >> "de\nwielen"
 * TODO: Split on last space | ("de wielen van", 12) >> "de wielen\nvan"
 * TODO: Split sentence | ("de wielen van de bus gaan rond en rond", 3) >> "de\nwie\nlen\nvan\nde\nbus\ngaa\nn\nron\nd\nen\nron\nd"
 */

public class WordWrap_should {

    private WordWrap wordWrap;

    @Before
    public void setUp() throws Exception {
        wordWrap = new WordWrap();
    }

    @Test
    public void return_single_word() throws Exception {
        assertThat(wordWrap.parse("de", 2), is("de"));
    }

    @Test
    public void return_split_word() throws Exception {
        assertThat(wordWrap.parse("de", 1), is("d\ne"));
    }

    @Test
    public void replace_space_with_newline() throws Exception {
        assertThat(wordWrap.parse("de de", 2), is("de\nde"));
    }

    @Test
    public void split_on_space() throws Exception {
        assertThat(wordWrap.parse("de wielen", 6), is("de\nwielen"));
    }

    @Test
    public void split_on_last_space() throws Exception {
        assertThat(wordWrap.parse("de wielen van", 12), is("de wielen\nvan"));
    }

    @Test
    public void split_sentence() throws Exception {
        assertThat(wordWrap.parse("de wielen van de bus gaan rond en rond", 3),
                is("de\nwie\nlen\nvan\nde\nbus\ngaa\nn\nron\nd\nen\nron\nd"));
    }
}
