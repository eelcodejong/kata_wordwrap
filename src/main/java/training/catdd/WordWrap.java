package training.catdd;

/**
 * Created by jongd on 10-May-17.
 */
public class WordWrap {

    private String right_sentence;
    private String left_sentence;
    private String result_sentence = "";

    public String parse(String sentence, int column) {
        if (sentence.length() > column) {
            left_sentence = get_left_sentence(sentence, column);
            right_sentence = get_right_sentence(sentence, left_sentence);
            sentence = right_sentence;
            result_sentence += left_sentence + "\n" + parse(sentence, column);
        } else {
            result_sentence += sentence;
        }
        return result_sentence;
    }

    private String get_right_sentence(String sentence, String left_sentence) {
        right_sentence = sentence.substring(left_sentence.length());
        right_sentence = remove_leading_space(right_sentence);
        return right_sentence;
    }

    private String get_left_sentence(String sentence, int column) {
        left_sentence = sentence.substring(0, column);
        if (contains_space(left_sentence)) {
            left_sentence = split_on_space(left_sentence, column);
        }
        return left_sentence;
    }

    private String split_on_space(String sentence, int column) {
        left_sentence = sentence.substring(0, sentence.lastIndexOf(" "));
        return left_sentence;
    }

    private boolean contains_space(String sentence) {
        return sentence.contains(" ");
    }

    private String remove_leading_space(String right_sentence) {
        if (right_sentence.startsWith(" ")) {
            right_sentence = right_sentence.trim();
        }
        return right_sentence;
    }
}
